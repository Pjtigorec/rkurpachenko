package com.RKurpachenko;

import com.RKurpachenko.ForSecondTask.Function;
import com.RKurpachenko.ForSecondTask.Factorial;
import com.RKurpachenko.ForSecondTask.Fibonacci;

public class SecondTask {
    private Function function;

    public  SecondTask(String [] args){

        int algoritmId = Integer.parseInt(args[0]);

        if (algoritmId == 1){
            function= new Fibonacci();
            function.calculating(args);
        }
        if (algoritmId == 2){
            function = new Factorial();
            function.calculating(args);
        }
    }
}
