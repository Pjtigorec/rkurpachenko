package com.RKurpachenko.typeOfHero;

public class Hero {

    protected int health;
    protected double luck;

    public Hero() {
        health = 100;
        luck = 0.5;
    }

    public boolean trapСalculation(double damage) {

        if ((damage - luck) <= 0.5){
            health -= 50;
            return true;
        }
        else {
            return false;
        }
    }

    public int getHealth() {
        return health;
    }

    public double getLuck() {
        return luck;
    }
}
