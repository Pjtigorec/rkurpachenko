package com.rkurpachenko.creditCard;

public interface Refill {
    boolean refill(int sum);
}
