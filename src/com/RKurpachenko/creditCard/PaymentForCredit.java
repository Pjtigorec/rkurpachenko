package com.rkurpachenko.creditCard;

public interface PaymentForCredit {
    boolean payment(int sum);
}
