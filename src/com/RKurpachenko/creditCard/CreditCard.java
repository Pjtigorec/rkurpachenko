package com.rkurpachenko.creditCard;

import com.rkurpachenko.Card;

public class CreditCard extends Card implements PaymentForCredit, Refill {

    public CreditCard(String name) {
        super(name);    }

    public CreditCard(String name, int balance) {
        super(name, balance);
    }

    @Override
    public boolean payment(int sum) {
        //Например, когда у нас есть процент от долга
        if (checkBalance(getBalance())) {
            setBalance(getBalance() - sum);
        }
        else setBalance((int) Math.round((getBalance() - sum) * 0.9));
        return true;
    }

    @Override
    public boolean refill(int sum) {
        //Например, когда у нас есть процент от долга
        if (checkBalance(getBalance())){
            setBalance(getBalance() + sum);
        }
        else setBalance((int) Math.round((getBalance() + sum) * 0.9));
        return true;
    }
}
