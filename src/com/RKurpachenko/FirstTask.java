package com.RKurpachenko;

public class FirstTask {

    public double calculating(String [] args){

        try {

            double denominator = Math.pow(Double.parseDouble(args[1]), 2) * (Double.parseDouble(args[2])+ Double.parseDouble(args[3]));
            double numerator = 4 * Math.pow(Math.PI, 2) * Math.pow(Double.parseDouble(args[0]), 3);
            if (denominator != 0){
                return Math.round(numerator/denominator);
            }
            else return 0;

        }catch (Exception ex){
            System.out.println(ex.getMessage());
            return 0;
        }
    }
}
