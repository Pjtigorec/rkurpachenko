package com.RKurpachenko.ForSecondTask;

public class Factorial extends Function {

    @Override
    protected String loopWhile (int n ) {
        try {
            int number = n, anser = 1;
            while (number >= 1){
                anser = anser * number;
                number--;
            }
            return "Факториал: " + anser;
        }catch (Exception ex){
            System.out.print(ex.getMessage());
            return "";
        }
    }

    @Override
    protected String loopDoWhile (int n) {
        try {
            int number = n, anser = 1;
            do{
                anser = anser * number;
                number--;
            }while (number >= 1);
            return "Факториал: " + anser;
        }catch (Exception ex){
            System.out.print(ex.getMessage());
            return "";
        }
    }

    @Override
    protected String loopFor (int n) {
        try {
            int anser = 1;
            for (int i = 1; i<n; i++){
                anser = anser * i;
            }
            return "Факториал: " + anser;
        }catch (Exception ex){
            System.out.print(ex.getMessage());
            return "";
        }
    }
}
