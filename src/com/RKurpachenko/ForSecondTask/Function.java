package com.RKurpachenko.ForSecondTask;

public class Function {

    String anser="";

    public void calculating(String [] args){

        if(convertToInt(args[1]) == 1){
            System.out.print(loopWhile(convertToInt(args[2])));
        }
        if(convertToInt(args[1]) == 2){
            System.out.print(loopDoWhile(convertToInt(args[2])));
        }
        if(convertToInt(args[1]) == 3){
            System.out.print(loopFor(convertToInt(args[2])));
        }
    }

    int convertToInt(String s) {
        return Integer.parseInt(s);
    }

    protected String loopWhile (int n ) {
        return  anser + n;
    }

    protected String loopDoWhile ( int n ) {
        return  anser + n;
    }

    protected String loopFor ( int n ) {
        return  anser + n;
    }
}
