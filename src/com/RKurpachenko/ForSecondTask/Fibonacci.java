package com.RKurpachenko.ForSecondTask;

public class Fibonacci extends Function {

    @Override
    protected String loopWhile (int n) {
        String anser = "";
        try {

            if(n == 1)
                anser = "1";

            if(n == 2){
                int i = 1;
                while (i <= 2){
                    anser = anser + " 1";
                    i++;
                }
            }
            if (n > 2) {
                int i = 1;
                while (i <= 2){
                    anser = anser + " 1";
                    i++;
                }
                int firstNumber = 1, secondNumber = 1;
                while (i<=n){
                    int number = firstNumber + secondNumber;
                    anser = anser + " " + number;
                    firstNumber = secondNumber; secondNumber = number; i++;
                }
            }
            return  anser;
        }catch (Exception ex){
            System.out.print(ex.getMessage());
            return "";
        }
    }

    @Override
    protected String loopDoWhile (int n) {
        String anser = "";
        try {
            if(n == 1)
                anser="1";

            if(n == 2){
                int i = 1;
                do {
                    anser = anser + " 1";
                    i++;
                }while (i <= 2);
            }

            if ( n > 2){
                int i = 1;
                do {
                    anser = anser + " 1";
                    i++;
                }while (i <= 2);

                int firstNumber = 1, secondNumber = 1;
                do{
                    int number = firstNumber + secondNumber;
                    anser = anser + " " + number;
                    firstNumber = secondNumber; secondNumber = number; i++;
                }while (i <= n);
            }
            return anser;
        }catch (Exception ex){
            return "";
        }
    }

    @Override
    protected String loopFor ( int n ) {
        String anser = "";
        try {
            if(n == 1)
                anser="1";

            if(n == 2){
                for(int i=0; i<2; i++)
                    anser = anser + " 1";
            }
            if (n > 2) {
                for(int i = 0; i < 2; i++)
                    anser = anser+" 1";

                int firstNumber = 1, secondNumber = 1;
                for (int i = 2; i < n; i++){
                    int number = firstNumber + secondNumber;
                    anser = anser + " " + number;
                    firstNumber=secondNumber; secondNumber=number;
                }
            }
            return anser;
        }catch (Exception ex){
            return "";
        }
    }
}
