package com.RKurpachenko.Persons;

import com.RKurpachenko.TypeOfHero.Hero;
import com.RKurpachenko.TypeOfHero.Lucky;
import com.RKurpachenko.TypeOfHero.Tank;

import java.util.Random;

public class Player {

    protected String name;
    protected Hero hero;

    public Player(String name, String type) {

        if (name.equals(""))
            this.name = "Чувачок";
        else this.name = name;

        if (type == "Танк") {
            hero = new Tank();
        }
        if (type == "Везунчик") {
            hero = new Lucky();
        }
        else {
            hero = new Hero();
        }
    }

    public void trapped (double damage) {
       if(hero.trapСalculation(damage))
           System.out.println("Ох, как бахнуло!");
       else System.out.println("Повезло!");

    }

    public int checkHealth() {
        return hero.getHealth();
    }
}
