package com.rkurpachenko;

public class Card {

    private String name;
    private int balance;

    public Card(String name) {
        this.name = name;
    }
    public Card(String name, int balance) {
        this.name = name;
        this.balance = balance;
    }

    public String getName() {
        return name;
    }
    public int getBalance() {
        return balance;
    }
    public void setBalance(int sum) {
        balance = sum;
    }

    public boolean checkBalance(int newBalance) {
        if(newBalance >= 0){
            return true;
        }
        else return false;
    }

    public boolean payment(int sum) {
        balance -= sum;
        return true;
    }

    public boolean refill(int sum) {
        balance += sum;
        return true;
    }
}
