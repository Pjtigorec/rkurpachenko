package com.rkurpachenko.debitCard;

import com.rkurpachenko.Card;

public class DebitCard extends Card implements PaymentForDebit {

    public DebitCard(String name) {
        super(name);
    }
    public DebitCard(String name, int balance) {
        super(name, balance);
    }

    @Override
    public boolean payment(int sum) {
        if(checkBalance(getBalance() - sum)){
            setBalance(getBalance() - sum);
            return true;
        }
        else return false;
    }
}
