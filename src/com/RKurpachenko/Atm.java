package com.rkurpachenko;

public class Atm {

    private Card card;

    public Atm(Card card) {
        this.card = card;
    }

    public void getMoney(int sum) {
        card.payment(sum);
    }

    public void setMoney(int sum) {
        card.refill(sum);
    }

    public String greeting() {
        return card.getName();
    }

    public String lookBalance() {
        return String.valueOf(card.getBalance());
    }
}
