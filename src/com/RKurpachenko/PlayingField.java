package com.RKurpachenko;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.ThreadLocalRandom;

public class PlayingField {

    private Player player;
    private String[][] field;
    private int positionI = 1;
    private int positionJ = 1;
    private boolean check = true;

    public PlayingField (Player player) {
        this.player = player;
        field = new String[12][12];
        firstStroke();
        onScreen();
    }

    public boolean anser () {

        if (health() <= 0) {
            return gameOverYouDead();
        }
        if (positionI != 11 && positionJ != 11) {
            check = stroke();
        }
        if (gameOver()) {
            return false;
        }
        return check;
    }

    private boolean stroke() {
        changePosition();
        onScreen();
        return true;
    }

    private void firstStroke() {

        field[0][0] = "/";
        field[0][11] = "\\";
        field[11][0] = "\\";
        field[11][11] = "/";
        for (int i = 1; i < 11; i++) {
            field[0][i] = "-";
            field[11][i] = "-";
            field[i][0] = "|";
            field[i][11] = "|";
            for(int j = 1; j < 11; j++) {
                field[i][j] = "*";
            }
        }
        field[1][1] = "Y";
        field[10][10] = "П";
        putTrap();
    }

    private int getDirection() {
        int direction = 0;
        System.out.println("Здоровье: " + player.checkHealth());
        try {
            System.out.println("Ход: ");
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            if (br.readLine().equals("") || br.readLine().length() > 1){
                direction = 5;
            }
            else {
                direction = Integer.parseInt(br.readLine());
            }
        } catch (IOException ex) {
            System.out.println("Ошибка ввода!");
            direction = 5;
        }
        return direction;
    }

    private void changePosition() {
        int direction = getDirection();
        if (direction == 8){
            if (positionI > 1) {
                positionI--;
                if (checkTrap()) {
                    field[positionI + 1][positionJ] = "*";
                }
                else {
                    field[positionI +1][positionJ] = "*";
                }
            }
        }
        if (direction == 2){
            if (positionI < 10) {
                positionI++;
                if (checkTrap()) {
                    field[positionI - 1][positionJ] = "*";
                }
                else {
                    field[positionI -1][positionJ] = "*";
                }
            }
        }
        if (direction == 4){
            if (positionJ > 1) {
                positionJ--;
                if (checkTrap()) {
                    field[positionI][positionJ + 1] = "*";
                }
                else {
                    field[positionI][positionJ +1] = "*";
                }
            }
        }
        if (direction == 6){
            if (positionJ < 10) {
                positionJ++;
                if (checkTrap()) {
                    field[positionI][positionJ - 1] = "*";
                }
                else {
                    field[positionI][positionJ -1] = "*";
                }
            }
        }
    }

    private void putTrap() {
        for (int i = 2; i < 10; i++) {
            int pos = ThreadLocalRandom.current().nextInt(2, 10 + 1);
            field[i][pos] = "o";
       }
    }

    private boolean checkTrap() {
        if (field[positionI][positionJ].equals("o")){
            System.out.println("Ловушка!");
            player.trapped(ThreadLocalRandom.current().nextDouble(0.1, 1 + 1));
            return false;
        }
        else return true;
    }

    private void onScreen() {
        field[positionI][positionJ] = "Y";
        for ( int i = 0; i < 12; i++) {
            for (int j = 0; j < 12; j++){
                System.out.print(field[i][j]);
            }
            System.out.println(" ");
        }
    }

    private int health() {
        return player.checkHealth();
    }

    private boolean gameOverYouDead() {
        System.out.println("Та та, та-та, тааа, та-та, та-та, та та");
        return false;
    }

    private boolean gameOver() {
        if(positionI == 10 && positionJ == 10) {
            System.out.println("Принцесса найдена!");
            return true;
        }
        else {
            return false;
        }
    }
}
