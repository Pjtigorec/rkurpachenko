package com.RKurpachenko

import org.testng.Assert
import org.testng.annotations.Test

class FirstTaskTest {

    @Test
    void testCalculating() {

        FirstTask ft = new FirstTask()
        double actual = ft.calculating("1", "1", "5", "1")
        double expected = 7.0

        Assert.assertEquals(expected, actual)
    }
}
