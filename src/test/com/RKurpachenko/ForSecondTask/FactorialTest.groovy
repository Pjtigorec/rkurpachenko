package com.RKurpachenko.ForSecondTask

import org.testng.Assert
import org.testng.annotations.Test

class FactorialTest {

    @Test
    void testCalculating() {

        Factorial fc = new Factorial()
        String actual = fc.loopFor(5)
        String expected = "Факториал: 24"

        Assert.assertEquals(expected, actual)
    }
}
