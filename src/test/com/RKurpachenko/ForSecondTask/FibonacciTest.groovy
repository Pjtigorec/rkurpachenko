package com.RKurpachenko.ForSecondTask

import org.testng.Assert
import org.testng.annotations.Test

class FibonacciTest {

    @Test
    void testCalculating() {

        Fibonacci fb = new Fibonacci()
        String actual = fb.loopWhile(5)
        String expected = " 1 1 2 3 5"

        Assert.assertEquals(expected, actual)
    }
}
